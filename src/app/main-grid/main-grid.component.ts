import { Component, ViewChild, Input, OnInit, TemplateRef, ViewContainerRef, AfterViewChecked, AfterViewInit } from '@angular/core';
import { Color } from 'src/entity/Color';
import { GridComponent } from '../grid/gird.component';
import { ColorScheme } from '../color-scheme/color-scheme.component';
import { HttpClientService } from 'src/service/http-client.service';
import { Pixel } from 'src/entity/pixel';
import { bddPixel } from 'src/entity/bddpixel';
import { COLORSCHEME } from 'src/assets/mocks/mock-color-scheme';
import { SharedService } from 'src/service/shared-service';
import { AuthService } from 'src/service/auth.service';

@Component({
  selector: 'app-main-grid',
  templateUrl: 'main-grid.component.html',
  styleUrls: [
    './main-grid.component.css',
  ]
})
export class MainGridComponent implements OnInit, AfterViewInit {


  // @ViewChild("gridoutlet", {read: ViewContainerRef}) 
  // gridOutlet: ViewContainerRef;
  // @ViewChild("gridcontent", {read: TemplateRef}) 
  // gridContentRef: TemplateRef<GridComponent>;

  @ViewChild(GridComponent, {static: false})
  pixelGrid: GridComponent;
  
  @ViewChild(Color, {static: false})
  colorScheme: ColorScheme;

  isLoged: boolean = false;

  color: [Color|undefined, number|undefined];

  constructor(
    private httpClientService: HttpClientService,
    private sharedService: SharedService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    //console.log(this.authService.getUserFromStorage());
    
    if (this.authService.getUserFromStorage() !== null) {
      this.isLoged = true;
    }
  }

  ngAfterViewInit() {
    if (this.isLoged) {
      this.httpClientService.get<Array<Object>>('/pixels').subscribe((data) => {
        //console.log( data );
        let pxs = new Array<Pixel>;
        const pixels = data as Array<bddPixel>;
        //console.log(pixels);
        for(let pix of pixels ) {
          //if( )
          let px : Pixel = new Pixel();
          px.id = pix.id;
          px.x = pix.x_coord;
          px.y = pix.y_coord;
          px.color = COLORSCHEME.at(pix.color) != undefined ? COLORSCHEME.at(pix.color) as Color : new Color(255,0,0);
          px.colorId = pix.color;
          px.modifiedAt = pix.last_modified_at;
          px.modifiedBy = pix.user_id.toString();
          
          pxs.push( px );
        }
        this.pixelGrid.setPixels( pxs );
        //console.log(this.pixelGrid.pixelGrid);
      });
    }
  }

  onClearGrid() {
    this.pixelGrid.clearPixels();
    //this.rerender();
  }

  setSelectedColor(color: [Color,number]) {
    this.color = color;
  }


  setLogStatus(isLog: boolean) {
    if( isLog != undefined )
      this.isLoged = isLog;
    else
      this.isLoged = false;
    //location.reload();
  }
  
  // rerender() {
  //   this.gridOutlet.clear();
  //   this.gridOutlet.createEmbeddedView(this.gridContentRef);
  // }
}
