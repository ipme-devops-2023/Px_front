import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User, createEmailValidator, createStrongPasswordValidator } from 'src/entity/user';
import { SharedService } from 'src/service/shared-service';
import { HttpClientService } from 'src/service/http-client.service';
import { Observable } from 'rxjs';
import { AuthService } from 'src/service/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent {

  @Input()
  user: User = {
    login: '',
    password: '',
    email: '',
  };

  onError: boolean = false;

  form: FormGroup = new FormGroup(
    {
      login: new FormControl(this.user.login,[ 
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(32),
      ]),
      password: new FormControl(this.user.password,[
        Validators.required,
        createStrongPasswordValidator(),                            
      ])    
    }
  )

  loginErrorMsg = "Le nom d'utilisateur est obligatoire et doit contenir entre 3 et 32 caractères.";
  passwordErrorMsg =  ['Le mot de passe est obligatoire.',// 0
                    'Il doit également contenir :', // 1
                    ' au minimum 8 charactères', // 2
                    ' au maximum 32 charactères', // 3
                    ' au moins une lettre en minuscule (a-z),\n', // 4
                    ' au moins une lettre en Majuscule (A-Z),\n', // 5
                    ' au moins un chiffre (0-9),\n', // 6
                    ' au moins un caractère spécial']; // 7
  
  constructor( private router: Router, 
               private sharedService: SharedService, 
               private httpClientService: HttpClientService, 
               private authService: AuthService ) { }

  getFormControl(key: string): FormControl {
    return this.form.get(key) as FormControl;
  }
  
  isFieldInvalid(key: string): boolean {
    const field: FormControl = this.getFormControl(key);
    return field.invalid && (field.touched || field.dirty);
  }

  onLogin(): void {
    if (this.form.valid) {
      this.user = this.form.value;
      // @TODO : envois à la base pour validation de la tentative de collection
      
      let loginUser = {
        "username": this.user.login,
        "password": this.user.password
      };

      let requestResponse = this.httpClientService.post<any>('/login/', loginUser)
      .subscribe(
        {
          next: (data) => {
            this.authService.addUserToStorage(data);
            this.router.navigate(['/game']);
            //if( data.valueOf())
          },
          error: (err: HttpErrorResponse) => {
            this.onError = true;            
          },
        }
      );
      //console.log( requestResponse );
    }
  }

}

