import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SigninComponent } from './signin.component';
import { FormControl } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


describe('TestComponent', () => {
  let component: SigninComponent;
  let fixture: ComponentFixture<SigninComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SigninComponent],
      imports: [HttpClientModule]
    });
    fixture = TestBed.createComponent(SigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

    it('should show form', () => {
      expect(component.getFormControl('login')).toEqual(new FormControl());
      expect(component.getFormControl('email')).toThrowError();
    });

});
