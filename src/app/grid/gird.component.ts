import { Component, OnInit, ViewChildren, QueryList, Input, HostListener, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
// import { PixelFillDirective } from '../pixel/pixel-fill.directive';
// import { PixelService } from '../pixel/pixel.service';
import { Pixel } from '../../entity/pixel';
import { HttpClientService } from 'src/service/http-client.service';
import { Color } from '../../entity/Color';
import { environment } from 'src/environment/environment.prod';
import { ColorScheme } from '../color-scheme/color-scheme.component';
import { NONE_TYPE } from '@angular/compiler';
import { COLORSCHEME } from 'src/assets/mocks/mock-color-scheme';
import { SharedService } from 'src/service/shared-service';
import { bddPixel } from 'src/entity/bddpixel';
import { AuthService } from 'src/service/auth.service';
import { UserStored } from 'src/entity/user';

@Component({
    selector: 'pixel-grid',
    templateUrl: `./grid.component.html`,
    styleUrls: [`./grid.component.css`],
    //changeDetection: ChangeDetectionStrategy.OnPush
  })
export class GridComponent implements OnInit{
  readonly PIXEL_SIZE = 20;
  width: number = 40;
  height: number = 30;
  createdAt: Date;
  pixelGrid: Array<Array<Pixel>>;
  user: UserStored | null;

  @Input()
  color: [Color|undefined, number|undefined];

  rgb: string|undefined;
 
  constructor( private httpClientService: HttpClientService, private authService: AuthService ) {}

  ngOnInit(): void {
    this.emptyGrid();
    this.user = this.authService.getUserFromStorage();
  }


  clearPixels(){
     this.emptyGrid();
  }

  createGrid( width: number = 10, height: number = 10 ) {
    this.width = width > 0 ? width : this.width ;
    this.height = height > 0 ? height : this.height;
    this.emptyGrid() ;
  }

  setPixel( pixel: Pixel) {
    if( 0 < pixel.x && pixel.x < this.width && 0 < pixel.y && pixel.y < this.height ) {
      this.pixelGrid[pixel.y][pixel.x] = pixel;
      //console.log(pixel);
    }
  }

  setPixels( pixels: Array<Pixel>) {
    //console.log(pixels[0].color);
    for( let pixel of pixels) {
      if( -1 < pixel.x && pixel.x < this.width && -1 < pixel.y && pixel.y < this.height ) {
        this.pixelGrid[pixel.y][pixel.x] = pixel;
        //console.log(pixel);
      }else{
        //console.log( 'pixel : ' + pixel + ' does not exist in the grid : ' + this.pixelGrid[pixel.x][pixel.y] );
      }
    }
  }

  getPixel( x: number, y: number) : Pixel|undefined {
    if( 0 < x && x < this.width && 0 < y && y < this.height ) {
      return this.pixelGrid[y][x] ;
    }else{
      return undefined;
    }
  }

  fillPixel(pixel: Pixel): void {
    if (this.color[0] && this.color[1] != undefined && this.color[1]> -1 ) {
      pixel.color = this.color[0];
      pixel.colorId = this.color[1];
      let Px : bddPixel =
      {
        "id": pixel.id,
        "x_coord": pixel.x,
        "y_coord": pixel.y,
        "color": pixel.colorId,
        "last_modified_at": Date.now().toString(),
        "user_id": this.user !== null ? this.user.id : -1,
      };
      console.log(Px);
      
      this.httpClientService.put<Object>('/pixels/'+pixel.id.toString(), Px).subscribe((data) => {
        //alert(data);
      });
    }     
  }

  emptyGrid(): void {
    this.pixelGrid = new Array(this.height);
      for(let i = 0; i < this.height; i++) {
          // this.pixelGrid[i] = new Array(this.width).fill(0);
          this.pixelGrid[i] = new Array(this.width);
          for(let j = 0; j < this.width; j++) {
            let px = new Pixel();
            px.id = j + i*this.width;
            px.x = j;
            px.y = i;
            px.color = new Color();
            px.colorId = 0;
            px.modifiedAt = new Date().toLocaleDateString();
            px.modifiedBy = 'auto';

            this.pixelGrid[i][j] = px;
          }
      }
  }

  fillGrid( pixels: Array<Pixel> ): void {
    //this.httpClientService.get<Object>('/pixels').subscribe((data) => {
      //alert(data);
    //  console;log(data);
      for( let px  of pixels )
      {
        if( px.x > 0 && px.x < this.width && px.y > 0 && px.y < this.height ) {
          this.pixelGrid[px.y][px.x] = px ;
        }
      }
    //});
      // this.pixelGrid = new Array(this.height);
      // for(let i = 0; i < this.height; i++) {
      //     this.pixelGrid[i] = new Array(this.width);
      //     for(let j = 0; j < this.width; j++) {
      //       //if( )
      //       let px = new Pixel();
      //       px.id = j + i*this.width;
      //       px.x = j;
      //       px.y = i;
      //       px.color = new Color();
      //       px.colorId = 0;
      //       px.modifiedAt = new Date().toLocaleDateString();
      //       px.modifiedBy = 'auto';
            
      //       this.pixelGrid[i][j] = px;
      //     }
      // }
      /*/
      console.log(data);
      console.log(data.toString());
      let pxs = JSON.parse(data.toString());
      //let pxs = data;
      // console.log(pxs);
      // for( let props in pxs ) {
      //   console.log( props );
      // }
      for( let props of pxs ) {
          let pxId: number = eval( props ) ;
          if( pxId > 0 && pxId < this.height*this.width ){
            console.log( props );
            console.log("x_coord : " + props.x_coord );
            let px = new Pixel();
            px.id = pxId;
            px.x = props.x_coord;
            px.y = props.y_coord;
            px.colorId = props.color;
            //px.color = ColorScheme.getColor(px.colorId);

            this.pixelGrid[px.x][px.y] = px ;
          }
        }
        //*/
    //});
  }
}