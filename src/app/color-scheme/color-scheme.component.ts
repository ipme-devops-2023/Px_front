import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { COLORSCHEME } from '../../assets/mocks/mock-color-scheme';
import { Color } from '../../entity/Color'
// import { PixelService } from '../pixel/pixel.service';

@Component({
    selector: 'color-scheme',
    templateUrl: `./color-scheme.component.html`,
    styleUrls: [`./color-scheme.component.css`]
  })
export class ColorScheme implements OnInit {
    readonly COLOR_BOX_SIZE = 30;
    colors: Color[];
    selectedColor: Color | undefined;
    currentColorId: number = -1 ;
    
    @Output()
    selectedColorEmitter: EventEmitter<[Color,number]> = new EventEmitter();

    constructor(
        // private pixelService: PixelService
    ) { }

    ngOnInit(): void {
        this.colors = COLORSCHEME;
    }

    addColor( col : Color ) {
        this.colors.push( col ) ;
    }
    
    addColors( cols : Array<Color> ) {
        for( let color of cols ) {
         this.colors.push( color ) ;
        }
    }

    addSelectedColor(selectedColor: Color): void {
        this.selectedColor = selectedColor;
        this.currentColorId = this.colors.indexOf( selectedColor );
        this.selectedColorEmitter.emit([this.selectedColor, this.currentColorId]);
    }

    // selectColor( color: Color ) {
    //     this.pixelService.currentColor = color;
    //     
    //     this.currentColorId = this.getColorIndex(color);
    //     console.log( `color: rgb( ${color.red},${color.green},${color.blue}) is selected !` )
    // }

    getColorByIndex ( idx: number ): Color|undefined {
        return this.colors.at(idx);
    }

    getColorIndex ( col: Color ) : number|undefined {
        return this.colors.indexOf( col ) ;
    }

    rgb( r: number, g: number, b: number ):string {
        return `rgb(${r}, ${g}, ${b})`;
    }

}
