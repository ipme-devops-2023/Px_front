import { Component, EventEmitter, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { SharedService } from 'src/service/shared-service';
import { Router } from '@angular/router';
import { AuthService } from 'src/service/auth.service';


export type Sign = 'signin' | 'signup';

@Component({
  selector: 'app-root',
  templateUrl: `./app.component.html`,
  styleUrls: [
    `./app.component.css`
  ]
})

export class AppComponent implements OnInit {

  appTitle = 'Pixel War';

  sign: Sign = 'signin' ;

  isLogon: boolean = false;
  url: string = '';

  constructor(
    private sharedService: SharedService,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    if (this.authService.getUserFromStorage() !== null) {
      this.isLogon = true;
    }
    this.url = this.router.url;
    //console.log(this.url);
    
  }

  toggleSign(type: Sign) {
    this.sign = type; 
  }

  logout(): void {
    this.isLogon =  false;
    this.router.navigate(['/']);
    this.authService.clearUserFromStorage();
  }

}
