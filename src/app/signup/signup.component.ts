import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User, createEmailValidator, createStrongPasswordValidator } from 'src/entity/user';
import { SharedService } from 'src/service/shared-service';
import { HttpClientService } from 'src/service/http-client.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  
  user: User = {
    login: '',
    password: '',
    email: '',
  };

  form: FormGroup = new FormGroup(
    {
      login: new FormControl(this.user.login,[ 
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(32),
      ]),
      email: new FormControl(this.user.email,[
        Validators.required,
        createEmailValidator(),                            
      ]),
      password: new FormControl(this.user.password,[
        Validators.required,
        createStrongPasswordValidator(),                            
      ])    
    }
  )

  loginErrorMsg = "Le nom d'utilisateur est obligatoire et doit contenir entre 3 et 32 caractères.";
  passwordErrorMsg =  ['Le mot de passe est obligatoire.',// 0
                    'Il doit également contenir :', // 1
                    '  au minimum 8 charactères', // 2
                    '  au maximum 32 charactères', // 3
                    '  au moins une lettre en minuscule (a-z),\n', // 4
                    '  au moins une lettre en Majuscule (A-Z),\n', // 5
                    '  au moins un chiffre (0-9),\n', // 6
                    '  au moins un caractère spécial']; // 7
  

  constructor(private router: Router, private sharedService: SharedService, private httpClientService: HttpClientService ) { }

  getFormControl(key: string): FormControl {
    return this.form.get(key) as FormControl;
  }
  
  isFieldInvalid(key: string): boolean {
    const field: FormControl = this.getFormControl(key);
    return field.invalid && (field.touched || field.dirty);
  }

  onRegister(): void {
    let newUser = {
      "email": this.form.value.email,
      "username": this.form.value.login,
      "password": this.form.value.password,
      "is_active": true
    };
    this.httpClientService.post<Object>('/users/', newUser).subscribe((data) => {
      this.onLogin();
    });
  }

  onLogin(): void {
    if (this.form.valid) {
      this.user = this.form.value;
      // @TODO : envois à la base pour validation de la tentative de collection
      
      this.sharedService.emitChange(true);
      this.router.navigate(['/login']);
      //this.router.navigate(['/game']);
    }
  }

}
