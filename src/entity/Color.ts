/* modèle pour une couleur */
export class Color {

    constructor(
        public red: number = 255,
        public green: number = 255,
        public blue: number = 255
    ) {}
}