export interface bddPixel {
    id: number,
    x_coord: number,
    y_coord: number,
    color: number,
    last_modified_at: string,
    user_id: number 
}