import { Color } from './Color'

 /* modèle pour le pixel */
export class Pixel {
   private _id: number;
   x: number;
   y: number;
   color: Color;
   colorId: number;
   modifiedAt: string;
   modifiedBy: string;

   get id(): number {
      return this._id;
   }

   set id(id: number) {
      this._id = id;
   }

   get rgb(): string {
      return `rgba( ${this.color.red}, ${this.color.green}, ${this.color.blue}, 1)`;
   }

}

