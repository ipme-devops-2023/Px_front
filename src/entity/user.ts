import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export interface User {
    login: string;
    password: string;
    email: string;
}

export interface UserStored extends User {
    id: number;
}

export function createStrongPasswordValidator(): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {
        const value = control.value;

        if (!value) {
            return null;
        }

        const haveMinimumCharacters = /.{8,}/.test(value); // at least 8 characters
        const haveMaximumCharacters = value.length < 33; // maximum 32 characters (not working but don't know why ???!!!)
        const haveLowerCase = /[a-z]+/.test(value); // a minuscule character
        const haveUpperCase = /[A-Z]+/.test(value); // a majuscule character
        const haveNumeric = /[\d]+/.test(value); // a digit
        const haveSpecialChar = /[^\w^\d^\s]+/.test(value); // a non Word, non Digit and no space character => special char

        const passwordValid = haveMinimumCharacters && haveMaximumCharacters && haveUpperCase && haveLowerCase && haveNumeric && haveSpecialChar;
        const pwdStrength = {
            strongPassword:
            {
                minChar: haveMinimumCharacters,
                maxChar: haveMaximumCharacters,
                lowerCase: haveLowerCase,
                upperCase: haveUpperCase,
                numeric: haveNumeric,
                specialChar: haveSpecialChar
            }
        };

        return !passwordValid ? pwdStrength: null;
    }
}

export function createEmailValidator (): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {
        const value = control.value;

        if (!value) {
            return null;
        }

        const isValidMAil = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(value);

        const emailValid = isValidMAil ;
        const email = {
            mail:
            {
                validMail: isValidMAil
            }
        };

        return !emailValid ? email: null;
    }
}