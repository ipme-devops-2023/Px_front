import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
//import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  private _baseURL: string = 'http://20.8.184.212';//environment.apiUrl ;

  constructor(
    private httpClient: HttpClient
  ) { }

  get<T>(path: string): Observable<T> {
    //console.log( this._baseURL );
    //console.log( path );
    return this.httpClient.get<T>(this._baseURL+path,{responseType:'json'});
  }

  put<T>(path: string, data: T): Observable<T> {
    return this.httpClient.put<T>(this._baseURL+path, data );
  }

  post<T>(path: string, data: T): Observable<T> {
    return this.httpClient.post<T>(this._baseURL+path, data );
  }

}
