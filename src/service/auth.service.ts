
import { Injectable } from '@angular/core';
import { UserStored } from 'src/entity/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  addUserToStorage(user: UserStored): void {
    localStorage.setItem('USER', JSON.stringify(user));
  }

  getUserFromStorage(): UserStored|null {
    const jsonUser = localStorage.getItem('USER');
    if (null !== jsonUser) {
        return JSON.parse(jsonUser);
    }
    return null;
  }

  clearUserFromStorage(): void {
    localStorage.removeItem('USER');
  }

}
