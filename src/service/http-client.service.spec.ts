
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientService } from './http-client.service';
//import { environment } from 'src/environment/environment';



describe('HttpClientService', () => {
  let component: HttpClientService;
  let fixture: ComponentFixture<HttpClientService>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HttpClientService ],
      //imports: [ environment ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HttpClientService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});