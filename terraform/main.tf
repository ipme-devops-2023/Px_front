resource "azurerm_resource_group" "rg-px-war-front" {
    name     = "${var.project_name}-resources"
    location = "West Europe"
}

resource "azurerm_static_site" "px-war-site" {
  name                = "${var.project_name}-app"
  resource_group_name = azurerm_resource_group.rg-px-war-front.name
  location            = azurerm_resource_group.rg-px-war-front.location

}

output "px_war_front_deployment_token" {
  description = "Token for static site Angular Frontend : pixel war app"
  value       = azurerm_static_site.px-war-site.api_key
  sensitive   = true
}
