module.exports = {
    verbose: false,
    maxWorkers: 10,
    transformIgnorePatterns: ['/node_modules/(?!(uuid|.*ol|.*quick-lru|.*akita|.*\\.mjs$))'],
    moduleFileExtensions: ['js', 'json', 'ts'],
    transform: {
      '^.+\\.tsx?$': 'ts-jest'
    },
    testMatch: ['(**/*.spec.ts)'],
    testRunner: 'jest-jasmine2',
    testEnvironment: 'jsdom',
    coverageDirectory: './coverage',
    coverageReporters: ["json", "lcov", 'text', 'text-summary'],
    collectCoverageFrom: [
      "src/**/*.ts"
    ],
    coveragePathIgnorePatterns: [
      "/node_modules/",
      "/src/main.ts",
      "/environment/environment.ts",
      "/environment/environment.prod.ts",
      "/src/app/app-routing.module.ts",
      "/src/app/app.module.ts",
      "/src/assets/"
      ],
    collectCoverage: true,
    reporters: ['default',  ['jest-sonar', {
      outputName: 'test-report.xml',
    }]],
    clearMocks: true,
    silent: true,
    setupFiles: ['<rootDir>/src/environment/environment.ts'],
  }